package jtt809.pojo;

import com.sjx.jtt809.pojo.Response;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import io.netty.buffer.ByteBuf;

import java.util.Date;

/**
 * 接收车辆定位信息数量通知消息
 * 链路类型：从链路。
 * 消息方向:上级平台往下级平台
 * 业务类型标识:	DOWN_TOTAL_RECV_BACK_MSG.
 * 描述:上级平台向下级平台定星通知已经收到下级平台上传的车辆定位信息数量(如:每收到10,000 条车辆定位信息通知一次)，
 * 本条消息不需下级平台应答。
 * <p>
 * 注：采用 UTC 时间表示，如 2010-1-10 9:7:54 的 UTC 值为 1263085674，其在协议中表示为0x000000004B49286A.
 */
public class ResponseClientJtt809_0x9101 extends Response {

    /**
     * 开始时间-结束时间期间，共收到的车辆定位信息数量
     */
    private long dynamicInfoTotal;

    /**
     * 开始时间，用 UTC 时间表示
     */
    private long startTime;

    /**
     * 结束时间，用 UTC 时间表示
     */
    private long endTime;

    private String startDate;

    private String endDate;

    public long getDynamicInfoTotal() {
        return dynamicInfoTotal;
    }

    public void setDynamicInfoTotal(long dynamicInfoTotal) {
        this.dynamicInfoTotal = dynamicInfoTotal;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    @Override
    protected void decodeImpl(ByteBuf buf) {
        this.dynamicInfoTotal = buf.readUnsignedInt();
        this.startTime = buf.readLong();
        this.startDate = DateUtil.format(new Date(this.startTime * 1000), DatePattern.NORM_DATETIME_PATTERN);
        this.endTime = buf.readLong();
        this.endDate = DateUtil.format(new Date(this.endTime * 1000), DatePattern.NORM_DATETIME_PATTERN);
    }
}
