package jtt809.pojo;

import com.sjx.jtt809.util.ConstantJtt809Util;
import io.netty.buffer.ByteBuf;

import java.util.Calendar;

/**
 * 车辆动态信息交换业务
 * 链路类型：主链路
 * 消息方向：下级平台往上级平台
 * 业务数据类型标识：UP_EXG_MSG
 * 描述：下级平台向上级平台发送车辆动态信息交换业务数据包
 *
 * 具体描述：
 * 实时上传车辆定位信息消息
 * 子业务类型标识：UP_EXG_MSG_REAL_LOCATION
 * 描述：主要描述车辆的实时定位信息
 */
public class RequestJtt809_0x1202 extends RequestJtt809_0x1200_VehiclePackage {

    /**
     * 该字段标识传输的定位信息是否使用国家测绘局批准的地图保密插件进行加密。
     * 加密标识：1-已加密，0-未加密。
     */
    private byte encrypt;

    /**
     * 日月年（dmyy），年的表示是先将年转换成 2 为十六进制数，如 2009 标识为 0x070xD9.
     * 时分秒（hms）
     */
    private Calendar dateTime;

    /**
     * 经度，单位为 1*10^-6 度。
     */
    private double lon;

    /**
     * 纬度，单位为 1*10^-6 度。
     */
    private double lat;

    /**
     * 速度，指卫星定位车载终端设备上传的行车速度信息，为必填项。单位为千米每小时（km/h）。
     */
    private short vec1;

    /**
     * 行驶记录速度，指车辆行驶记录设备上传的行车速度信息，为必填项。单位为千米每小时（km/h）。
     */
    private short vec2;

    /**
     * 车辆当前总里程数，值车辆上传的行车里程数。单位为千米（km）。
     */
    private short vec3;

    /**
     * 方向，0-359，单位为度（。），正北为 0，顺时针。
     */
    private short direction;

    /**
     * 海拔高度，单位为米（m）
     */
    private short altitude;

    /**
     * 车辆状态，二进制表示，B31B30B29。。。。。。B2B1B0.具体定义按照 JT/T808-2011 中表 17 的规定
     * 位            状态
     * 0             0：ACC关；1：ACC开
     * 1             0：未定位；1：定位
     * 2             0：北纬；1：南纬
     * 3             0:东经；1：西经
     * 4             0：运营桩体；1：停运状态
     * 5             0：经纬度未经保密插件加密；1：经纬度已经保密插件加密
     * 6-9           保留
     * 10            0：车辆油路正常；1：车辆油路断开
     * 11            0：车辆电路正常；1：车辆电路断开
     * 12            0:车门解锁；1：车门加锁
     * 13-31         保留
     */
    private int state;

    /**
     * 报警状态，二进制表示，0 标识正常，1 表示报警：B31B30B29 。。。。。。 B2B1B0. 具 体定义按照 JT/T808-2011 中表 18 的规定
     *
     * 位	     定 义	                            处理说明
     * 0        1：紧急报警，触动报警开关后触发	    收到应答后清零
     * 1        1：超速报警	                        标志维持至报警条件解除
     * 2        1：疲劳驾驶	                        标志维持至报警条件解除
     * 3        1：预警	                            收到应答后清零
     * 4        l：GNSS模块发生故障                  标志维持至报警条件解除
     * 5        1：GNSS天线未接或被剪断	            标志维持至报警条件解除
     * 6        l：GNSS天线短路	                    标志维持至报警条件解除
     * 7        1：终端主电源欠压                    标志维持至报警条件解除
     * 8        1：终端主电源掉电                    标志维持至报警条件解除
     * 9        1：终端LCD或显示器故障               标志维持至报警条件解除
     * 10       1：TTS模块故障                       标志维持至报警条件解除
     * 11       1：摄像头故障                        标志维持至报警条件解除
     * 12-17    保留
     * 18       1：当天累计驾驶超时	                标志维持至报警条件解除
     * 19       1：超时停车                          标志维持至报警条件解除
     * 20       1：进出区域                          收到应答后清零
     * 21       1：进出路线                          收到应答后清零
     * 22       1：路段行驶吋间不足/过长              收到应答后清零
     * 23       1：路线偏离报警                      标志维持至报警条件解除
     * 24       1：车辆VSS故障                       标志维持至报警条件解除
     * 25       1：车辆油量异常                      标志维持至报警条件解除
     * 26       1：车辆被盗（通过车辆防盗器）         标志维持至报警条件解除
     * 27       1：车辆非法点火                      收到应答后清零
     * 28       1：车辆非法位移	                    收到应答后清零
     * 29       1：碰撞侧翻报警                      标志维持至报警条件解除
     * 30-31	保留
     */
    private int alarm;

    public RequestJtt809_0x1202() {
        super(ConstantJtt809Util.UP_EXG_MSG_REAL_LOCATION);
        this.dataLength = 36;
    }

    public byte getEncrypt() {
        return encrypt;
    }

    public void setEncrypt(byte encrypt) {
        this.encrypt = encrypt;
    }

    public Calendar getDateTime() {
        return dateTime;
    }

    public void setDateTime(Calendar dateTime) {
        this.dateTime = dateTime;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public short getVec1() {
        return vec1;
    }

    public void setVec1(short vec1) {
        this.vec1 = vec1;
    }

    public short getVec2() {
        return vec2;
    }

    public void setVec2(short vec2) {
        this.vec2 = vec2;
    }

    public short getVec3() {
        return vec3;
    }

    public void setVec3(short vec3) {
        this.vec3 = vec3;
    }

    public short getDirection() {
        return direction;
    }

    public void setDirection(short direction) {
        this.direction = direction;
    }

    public short getAltitude() {
        return altitude;
    }

    public void setAltitude(short altitude) {
        this.altitude = altitude;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getAlarm() {
        return alarm;
    }

    public void setAlarm(int alarm) {
        this.alarm = alarm;
    }

    @Override
    protected void encodeDataImpl(ByteBuf buf) {
        // 1 byte
        buf.writeByte(getEncrypt());

        // 日月年 dmyy 4 byte
        buf.writeByte((byte) getDateTime().get(Calendar.DATE));
        buf.writeByte((byte) (getDateTime().get(Calendar.MONTH) + 1));
        String hexYear = String.format("%04x",getDateTime().get(Calendar.YEAR));
        buf.writeBytes(hexStringToByte(hexYear));

        // 时分秒
        buf.writeByte((byte) getDateTime().get(Calendar.HOUR));
        buf.writeByte((byte) getDateTime().get(Calendar.MINUTE));
        buf.writeByte((byte) getDateTime().get(Calendar.SECOND));

        // 经度 4 byte
        buf.writeInt(formatLonLat(getLon()));
        // 纬度 4 byte
        buf.writeInt(formatLonLat(getLat()));

        // 速度 2 byte
        buf.writeShort(getVec1());
        // 行驶记录速度 2 byte
        buf.writeShort(getVec2());
        // 车辆当前总里程数 4 byte
        buf.writeInt(getVec3());

        // 方向 2 byte
        buf.writeShort(getDirection());

        // 海拔 2 byte
        buf.writeShort(getAltitude());

        // 车辆状态
        buf.writeInt(getState());

        // 报警状态
        buf.writeInt(getAlarm());
    }
}
