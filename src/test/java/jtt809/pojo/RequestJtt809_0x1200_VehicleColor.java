package jtt809.pojo;

/**
 * 车辆状态枚举类
 */
public enum RequestJtt809_0x1200_VehicleColor {

    BLUE(1, "蓝色"),

    YELLOW(2, "黄色"),

    BLACK(3, "黑色"),

    WHITE(4, "白色"),

    OTHER(9, "其它");

    private int ret;

    private String msg;

    RequestJtt809_0x1200_VehicleColor(int ret, String msg) {
        this.ret = ret;
        this.msg = msg;
    }

    public int getRet() {
        return ret;
    }

    public void setRet(int ret) {
        this.ret = ret;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
