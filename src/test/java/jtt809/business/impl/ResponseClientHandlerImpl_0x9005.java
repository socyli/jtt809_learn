package jtt809.business.impl;

import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;
import io.netty.channel.ChannelHandlerContext;
import jtt809.business.IClientBusinessServer;
import jtt809.pojo.RequestClientJtt809_0x9006;
import jtt809.pojo.ResponseClientJtt809_0x9005;

/**
 * 从链路连接保持请求消息
 * 链路类型:从链路。
 * 消息方向:上级平台往下级平台。
 * 业务数据类型标识:DOWN_ LINKTEST_ REQ。
 * 描述:从链路建立成功后，上级平台向下级平台发送从链路连接保持请求消息，以保持
 * 从链路的连接状态。
 * 从链路连接保持请求消息，数据体为空。
 */
public class ResponseClientHandlerImpl_0x9005 implements IClientBusinessServer<ResponseClientJtt809_0x9005> {

    private static final Log logger = LogFactory.get();

    public void businessHandler(ChannelHandlerContext ctx, ResponseClientJtt809_0x9005 msg) {
        ctx.writeAndFlush(new RequestClientJtt809_0x9006());
    }
}
