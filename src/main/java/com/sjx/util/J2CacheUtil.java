package com.sjx.util;

import net.oschina.j2cache.CacheChannel;
import net.oschina.j2cache.J2Cache;

public class J2CacheUtil {
    /**
     * J2Cache实例
     */
    private static CacheChannel cache;

    /**
     * 单例模式获取J2Cache实例
     *
     * @return
     */
    public static CacheChannel getCacheInstance() {
        try {
            if (null == cache) {
                synchronized (PropsUtil.class) {
                    if (null == cache) {
                        cache = J2Cache.getChannel();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cache;
    }

    /**
     * 关闭cache链接
     */
    public static void close() {
        if (cache != null) {
            cache.close();
        }
    }
}
