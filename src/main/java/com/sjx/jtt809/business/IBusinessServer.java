package com.sjx.jtt809.business;

import io.netty.channel.ChannelHandlerContext;

/**
 * 业务接口
 */
public interface IBusinessServer<T> {

    /**
     * 业务处理入口
     *
     * @param ctx
     * @param msg
     */
    public void businessHandler(ChannelHandlerContext ctx, T msg) throws InterruptedException;
}
