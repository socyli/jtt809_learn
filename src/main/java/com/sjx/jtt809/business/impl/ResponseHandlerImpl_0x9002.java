package com.sjx.jtt809.business.impl;

import com.sjx.jtt809.business.IBusinessServer;
import com.sjx.jtt809.pojo.command.ResponseJtt809_0x9002;
import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;
import io.netty.channel.ChannelHandlerContext;

/**
 * 从链路连接应答信息
 * 链路类型:从链路。
 * 消息方问:下级平台往上级平台。
 * 业务数据类型标识:DOWN_CONNNECT_RSP。
 * 描述：下级平台作为服务器端向上级平台客户端返回从链路连接应答消息，上级平台在接收到该应答消息结果后，根据结果进行链路连接处理
 */
public class ResponseHandlerImpl_0x9002 implements IBusinessServer<ResponseJtt809_0x9002> {

    private static final Log logger = LogFactory.get();

    public void businessHandler(ChannelHandlerContext ctx, ResponseJtt809_0x9002 msg) throws InterruptedException {

        switch (msg.getResult()) {
            case 0:
                msg.setIsLoginFlagFromDownPlatform(true);
                logger.info("=====> 【上级平台|信息】下级平台登录成功！");
                break;
            case 1:
                msg.setIsLoginFlagFromDownPlatform(false);
                logger.info("=====> 【上级平台|信息】校验码错误！");
                break;
            case 2:
                msg.setIsLoginFlagFromDownPlatform(false);
                logger.info("=====> 【上级平台|信息】校验码错误！");
                break;
            default:
                msg.setIsLoginFlagFromDownPlatform(false);
                logger.info("=====> 【上级平台|信息】其它");
        }

    }

}
