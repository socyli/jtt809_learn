package com.sjx.jtt809.pojo;

import io.netty.channel.ChannelHandlerContext;

/**
 * 基础业务类
 */
public class BusinessBean {
    private ChannelHandlerContext ctx;

    private Response response;

    public BusinessBean(ChannelHandlerContext ctx, Response response) {
        this.ctx = ctx;
        this.response = response;
    }

    public ChannelHandlerContext getCtx() {
        return ctx;
    }

    public void setCtx(ChannelHandlerContext ctx) {
        this.ctx = ctx;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }
}
