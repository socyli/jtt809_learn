package com.sjx.jtt809.pojo.command;

import com.sjx.jtt809.pojo.BasePackage;
import com.sjx.jtt809.util.ConstantJtt809Util;
import io.netty.buffer.ByteBuf;

/**
 * 从链路连接请求消息
 * 链路类型:从链路。
 * 消息方向:上级平台往下级平台。
 * 业务数据类型标识:DOWN_CONNECT_REQ。
 * 描述:主链路建立连接后，上级平台向下级平台发送从链路连接清求消息，以建立从链路连接
 */
public class RequestJtt809_0x9001 extends BasePackage {

    private int verifyCode;

    public RequestJtt809_0x9001() {
        super(ConstantJtt809Util.DOWN_CONNECT_REQ);
        this.msgBodyLength = 4;
    }

    public int getVerifyCode() {
        return verifyCode;
    }

    public void setVerifyCode(int verifyCode) {
        this.verifyCode = verifyCode;
    }

    @Override
    protected void encodeImpl(ByteBuf buf) {
        // 4 byte
        buf.writeInt(getVerifyCode());
    }
}
