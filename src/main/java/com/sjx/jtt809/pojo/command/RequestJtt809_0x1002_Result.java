package com.sjx.jtt809.pojo.command;

/**
 * 主链路登录应答消息 枚举类
 */
public enum RequestJtt809_0x1002_Result {
    /**
     * 成功
     */
    SCUUESS(0x00, "成功"),

    /**
     * IP地址不正确
     */
    IP_ADDRESS_IS_ERROR(0x01, "IP地址不正确"),

    /**
     * 接入码不正确
     */
    ACCESS_CODE_IS_ERROR(0x02, "接入码不正确"),

    /**
     * 用户没有注册
     */
    USER_NOT_REGISTERED(0x03, "用户没有注册"),

    /**
     * 密码错误
     */
    WRONG_PASSWORD(0x04, "密码错误"),

    /**
     * 资源紧张，稍后再连接（已经占用）
     */
    RESOURCE_CRISIS(0x05, "资源紧张，稍后再连接（已经占用）"),

    /**
     * 其他
     */
    OTHER(0x06, "其他");

    private int ret;
    private String msg;

    private RequestJtt809_0x1002_Result(int ret, String msg) {
        this.ret = ret;
        this.msg = msg;
    }

    public int getRet() {
        return ret;
    }

    public void setRet(int ret) {
        this.ret = ret;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
