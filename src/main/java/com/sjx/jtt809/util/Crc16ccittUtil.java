package com.sjx.jtt809.util;

public class Crc16ccittUtil {

    /**
     * 从数据头到校验码前的 CRC 1 G-CCITT 的校验值，遵循人端排序方式的规定
     *
     * @param bytes
     * @return
     */
    public static short toCRC16_CCITT(byte[] bytes) {
        int crc = 0xFFFF;
        for (int j = 0; j < bytes.length; j++) {
            crc = ((crc >>> 8) | (crc << 8)) & 0xffff;
            crc ^= (bytes[j] & 0xff);//byte to int, trunc sign
            crc ^= ((crc & 0xff) >> 4);
            crc ^= (crc << 12) & 0xffff;
            crc ^= ((crc & 0xFF) << 5) & 0xffff;
        }
        crc &= 0xffff;
        return (short) crc;
    }
}
