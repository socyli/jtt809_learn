package com.sjx.jtt809.manager;

import com.sjx.jtt809.handler.initializer.PrimaryLinkServerJtt809Initializer;
import com.sjx.util.J2CacheUtil;
import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

/**
 * 主链路启动管理器
 */
public class PrimaryLinkManagerJtt809 implements Runnable {

    /**
     * 日志类
     */
    private static final Log logger = LogFactory.get();

    /**
     * 绑定IP
     */
    private String ip;

    /**
     * 绑定端口
     */
    private int port;

    /**
     * 构造函数
     *
     * @param ip
     * @param port
     */
    public PrimaryLinkManagerJtt809(String ip, int port) {
        this.ip = ip;
        this.port = port;
    }

    /**
     * 启动服务端
     */
    public void run() {
        EventLoopGroup bossGroup = new NioEventLoopGroup();
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            ServerBootstrap serverBootstrap = new ServerBootstrap();
            serverBootstrap.group(bossGroup, workerGroup)
                            .channel(NioServerSocketChannel.class)
                            // 保持长连接
                            .childOption(ChannelOption.SO_KEEPALIVE,true)
                            // 标识当服务器请求处理线程全满时，用于临时存放已完成三次握手的请求的队列的最大长度
                            .option(ChannelOption.SO_BACKLOG, 1024)
                            .childHandler(new PrimaryLinkServerJtt809Initializer());

            // 绑定端口，同步等待成功
            ChannelFuture channelFuture = serverBootstrap.bind(this.ip, this.port).sync();
            logger.info("==========> 上级平台服务端启动成功。绑定IP：{}，绑定端口：{}", this.ip, this.port);

            // 等待服务器监听端口关闭
            channelFuture.channel().closeFuture().sync();
        } catch (Exception e) {
            logger.info("==========> 上级平台服务端启动出错。错误：{}", e.getMessage());
            e.printStackTrace();
        } finally {
            // 释放线程池资源
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
            // 关闭缓存链接
            J2CacheUtil.close();
        }
    }
}
